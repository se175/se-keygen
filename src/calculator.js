// We first check the length of the Password!
// then we control the character set of the password and length of evert character set
// example

// An example
// The password "Summer2017" is 10 characters long, and uses upper and lowercase alphanumeric characters.
// The size of the character set for this password is 26 (upper case) + 26 (lower case) + 10 (numbers) = 62 characters.
// This means the number of guesses to guarantee we find the password is 62^10. This number is about equal to 2^59.5 and so "Summer2017" has 59.5 bits of entropy.
//  For those interested in maths, finding the bits of entropy is calculated by e = L * log(C)/log(2) where L is the length of the password and C is the size of the
//  character set.
// const calculateEntropy = (charset, length) =>
//   Math.round((length * Math.log(charset)) / Math.LN2);

// const stdCharsets = [
//   {
//     name: 'lowercase',
//     re: /[a-z]/, // abcdefghijklmnopqrstuvwxyz
//     length: 26,
//   },
//   {
//     name: 'uppercase',
//     re: /[A-Z]/, // ABCDEFGHIJKLMNOPQRSTUVWXYZ
//     length: 26,
//   },
//   {
//     name: 'numbers',
//     re: /[0-9]/, // 1234567890
//     length: 10,
//   },
//   {
//     name: 'symbols',
//     re: /[^a-zA-Z0-9]/, //  !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ (and any other)
//     length: 33,
//   },
// ];

const niceware = require('niceware');
const stringEntropy = require('fast-password-entropy');
const password = require('password');
const { generatePassphrase } = require('./generate');
const adjs = require('../res/adj');
const verbs = require('../res/verbs');
const nouns = require('../res/nouns');
// function generatePassphrase(length) {
//   return niceware.generatePassphrase(length);
// }

// // generate passphrase with given entropy (in bits)
// function generatePassphrase(length, entropy) {
//   return niceware.generatePassphrase(length, entropy);
// }

// console.log(stringEntropy('@hmet'));

// console.log(generatePassphrase(20));
// console.log(stringEntropy(generatePassphrase(10).join("")));

// English general sentence structure
// Subject + Verb + adjective + Object
// but we wont use the subject, at least not as i,you etc. but maybe animal names
// possible libraries
// Docker-Names - generates semi-random, easy to remember names similar
// niceware -  generating random-yet-memorable passwords,
// fast-password-entropy - calculates entropy of a string
// change-case - Transform a string between camelCase, PascalCase, Capital Case, snake_case, param-case, CONSTANT_CASE and others.
const setting = {
  numberOfWords: 5,
  isCapitalized: true,
  isSpecialChars: true,
  isNumbers: true,
  // number: 1071,
  // wieviele bits sollte ich haben?
  //number at the end
  // just remove a letter, or put extra letter
};
console.log(`adjectives  = ` + adjs.length);
console.log(`verbs length = ` + verbs.length);
console.log(`nouns length = ` + nouns.length);
console.log(generatePassphrase(setting));

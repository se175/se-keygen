const adjList = require('../res/adj');
const verbs = require('../res/verbs');
const nouns = require('../res/nouns');
const { changeCharactersWithProbability } = require('./modifier');
const fs = require('fs');

function createAdjectiveDictionary(words) {
  const adjectiveSet = new Set();
  const tempArray = [];
  words.map((adj) => {
    tempArray.push(adj);
  });
  for (let i = 0; i < 12; i++) {
    changeCharactersWithProbability(tempArray).forEach((word) => {
      tempArray.push(word);
    });
  }
  tempArray.forEach((word) => {
    adjectiveSet.add(word);
  });

  return adjectiveSet;
}

function writeToFile(filename, wordList) {
  fs.writeFile(filename, wordList.join('\n'), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
}

const newVerbs = [];
verbs.map((verb) => {
  newVerbs.push(verb.present);
  newVerbs.push(verb.past);
});
console.log(createAdjectiveDictionary(adjList).size); // => around 10429 unique words
console.log(createAdjectiveDictionary(newVerbs).size); // => around 7461 unique words
console.log(createAdjectiveDictionary(nouns).size); // => around 11532 unique words
// total = 10429 + 7461 + 11532 = 29422
// entropy  with 3 words = e = 3 * log(29422)/log(2) = 44 bits
// with random letter removal dictionary sizes are 589251,131025,600493
// entropy  with 3 words = e = 3 * log(29422)/log(2) =  60 bits

// with random letter removal  and adder dictionary sizes are 2069156,1098877,2091418
// entropy  with 3 words = e = 3 * log(29422)/log(2) =  66 bits

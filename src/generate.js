const adjs = require('../res/adj');
const verbs = require('../res/verbs');
const nouns = require('../res/nouns');
const { capitalize, changeCharactersWithProbability } = require('./modifier');
// generate password with given number of words and settings
function generatePassphrase(settings) {
  // first noun, then verb, then adjective, then noun, and repeat it
  let passphrase = [];
  for (let i = 0; i < settings.numberOfWords; i++) {
    if (i % 3 === 0) {
      passphrase.push(generateNoun());
    }
    if (i % 3 === 1) {
      passphrase.push(generateVerb());
    }
    if (i % 3 === 2) {
      passphrase.push(generateAdjective());
    }
  }
  return handleSettings(passphrase, settings);
}
function handleSettings(passphrase, settings) {
  let newPassphrase = passphrase;
  if (settings.isCapitalized) {
    newPassphrase = capitalize(passphrase);
  }
  if (settings.isSpecialChars) {
    newPassphrase = changeCharactersWithProbability(newPassphrase);
  }
  return newPassphrase;
}
function generateNoun() {
  return nouns[Math.floor(Math.random() * nouns.length)];
}
function generateVerb() {
  return verbs[Math.floor(Math.random() * verbs.length)].present;
}
function generateAdjective() {
  return adjs[Math.floor(Math.random() * adjs.length)];
}

module.exports = {
  generatePassphrase,
};

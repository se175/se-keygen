// Takes a list of words, and capitilize each word.
//@param {Array} words - list of words
function capitalize(words) {
  return words.map((word) => word[0].toUpperCase() + word.slice(1));
}

// Take a list of words, and change some letters of the words with special characters with a probability for each letter.
// For each letter, there is a 20% probability that it will be replaced with special character
//@param {Array} words - list of words
function changeCharactersWithProbability(words) {
  const substitudes = [
    [/a/, '@'],
    [/e/, '€'],
    [/E/, '3'],
    [/s/, '$'],
    [/B/, '8'],
    [/i/, '!'],
    [/o/, '0'],
    [/I/, '1'],
    [/O/, '0'],
    [/S/, '5'],
  ];
  return words.map((word) => {
    let newWord = word;
    for (let substitude of substitudes) {
      newWord =
        Math.floor(Math.random() * 100) < 20
          ? newWord.replace(substitude[0], substitude[1])
          : newWord;
    }
    newWord = randomLetterRemover(newWord);
    newWord = randomLetterAdder(newWord);

    return newWord;
  });
}
function randomLetterRemover(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length);
  letters.splice(randomIndex, 1);
  return letters.join('');
}
function randomLetterAdder(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length);
  letters.splice(randomIndex, 0, word[randomIndex]);
  return letters.join('');
}

module.exports = {
  capitalize,
  changeCharactersWithProbability,
};

// e = L * log(C)/log(2)
// length of the word
// c size of the character set

//without any special characters
// adjectives  = 961
// verbs length = 633
// nouns length = 993
// c = 961 + 633 + 993 =  2587
// L = 5
// e = 5 * log(2587)/log(2) = 56 bits

// with modifiers       961 * 10 * 2 = 2,567
// adjectives  = 961 -> only one letter,2 letters, 3 letters... up to 10 letters can change + capital and lowercase
// verbs length = 633
// nouns length = 993

// Accelerated
// accelerated
// @cceler@ted
// @cc3l3rat3d
// @cceler@ted
// @cc3l3rat3d
//
// for this word. 6 different options, but with different word combinations, the size can increase or decrease
